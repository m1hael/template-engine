
     H nomain

      /include 'tmpltokn_h.rpgle'
      /include 'tmpltkna_h.rpgle'


     P template_ast_token_abstract_getType...
     P                 B                   export
     D                 PI             3I 0
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       return data.type;
      /end-free
     P                 E


     P template_ast_token_abstract_setType...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   type                         3I 0 const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       data.type = type;
      /end-free
     P                 E


     P template_ast_token_abstract_getStart...
     P                 B                   export
     D                 PI            10I 0
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       return data.start;
      /end-free
     P                 E


     P template_ast_token_abstract_setStart...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   start                       10I 0 const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       data.start = start;
      /end-free
     P                 E


     P template_ast_token_abstract_getLength...
     P                 B                   export
     D                 PI            10I 0
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       return data.length;
      /end-free
     P                 E


     P template_ast_token_abstract_setLength...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   length                      10I 0 const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       data.length = length;
      /end-free
     P                 E


     P template_ast_token_abstract_getId...
     P                 B                   export
     D                 PI            20I 0
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_abstract_t)
     D                                     based(header.data)
      /free
       return data.id;
      /end-free
     P                 E

