
     H nomain

     D retrieveReferenceName...
     D                 PR           100A
     D   data                      1000A   const varying


      /include 'tmpltkna_h.rpgle'
      /include 'tmpltokn_h.rpgle'
      /include 'tmpltknr_h.rpgle'
      /include 'unicode_c.rpgle'
      /include 'libc_h.rpgle'

     D TEMPLATE_AST_TOKEN_REFERENCE_ID...
     D                 C                   'template_ast_token_ref'

     D template_ast_token_ref_t...
     D                 DS                  qualified based(nullPointer) align
     D   abstract                          likeds(template_ast_token_abstract_t)
     D   reference                     *
     D   quiet                         N
     D   refName                    100A


     P template_ast_token_ref_create...
     P                 B                   export
     D                 PI              *
     D   id                          20I 0 const
     D   start                       10I 0 const
     D   pData                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
     D null            S              1A   inz(x'00')
     D refData         S          65535A   based(pData)
      /free
       token = %alloc(%size(header));
       header.data = %alloc(%size(data));

       header.id = TEMPLATE_AST_TOKEN_REFERENCE_ID;
       header.proc_getId = %paddr('template_ast_token_abstract_getId');
       header.proc_setType = %paddr('template_ast_token_abstract_setType');
       header.proc_getType = %paddr('template_ast_token_abstract_getType');
       header.proc_setStart = %paddr('template_ast_token_abstract_setStart');
       header.proc_getStart = %paddr('template_ast_token_abstract_getStart');
       header.proc_setLength = %paddr('template_ast_token_abstract_setLength');
       header.proc_getLength = %paddr('template_ast_token_abstract_getLength');
       header.proc_setData = %paddr('template_ast_token_ref_setData');
       header.proc_getData = %paddr('template_ast_token_ref_getData');
       header.proc_finalize = %paddr('template_ast_token_ref_finalize');

       data.abstract.id = id;
       data.abstract.type = TEMPLATE_AST_TOKEN_TYPE_REFERENCE;
       data.abstract.start = start;
       data.abstract.length = strlen(pData);
       data.reference = %alloc(data.abstract.length + 1);
       memcpy(data.reference : pData : data.abstract.length + 1); // copy the ending null too
       memcpy(data.reference + data.abstract.length : %addr(null) : 1);
       data.refName = retrieveReferenceName(%str(pData));

       return token;
      /end-free
     P                 E


     P template_ast_token_ref_finalize...
     P                 B                   export
     D                 PI
     D   token                         *
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
      /free
       if (token <> *null);

         if (header.data <> *null);
           dealloc data.reference;
           dealloc header.data;
         endif;

         dealloc token;

       endif;
      /end-free
     P                 E


     P template_ast_token_ref_getData...
     P                 B                   export
     D                 PI         65535A   varying
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
      /free
       if (header.data <> *null);
         return %str(data.reference);
       else;
         return *blank;
       endif;
      /end-free
     P                 E


     P template_ast_token_ref_setData...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   pData                    65535A   const varying
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
     D value           S          65535A   varying
     D null            S              1A   inz(x'00')
      /free
       if (header.data <> *null);
         value = pData;

         if (data.reference = *null);
           data.reference = %alloc(%len(value) + 1);
         else;
           data.reference = %realloc(data.reference : %len(value) + 1);
         endif;

         // copy data
         memcpy(data.reference : %addr(value : *DATA) : %len(value));

         // add null
         memcpy(data.reference + %len(value) : %addr(null) : 1);

         // update header
         data.abstract.length = %len(value);
       endif;
      /end-free
     P                 E


     P template_ast_token_ref_getReferenceName...
     P                 B                   export
     D                 PI           100A   varying
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
      /free
       return %trimr(data.refName);
      /end-free
     P                 E


     P template_ast_token_ref_setQuiet...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   quiet                         N   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
      /free
       data.quiet = quiet;
      /end-free
     P                 E


     P template_ast_token_ref_isQuiet...
     P                 B                   export
     D                 PI              N
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_ref_t)
     D                                     based(header.data)
      /free
       return data.quiet;
      /end-free
     P                 E


     P retrieveReferenceName...
     P                 B
     D                 PI           100A
     D   data                      1000A   const varying
      *
     D tmp             S           1000A   varying
     D x               S             10I 0
     D name            S            100A
     D trimChars       S              4A
      /free
       trimChars = ' ' + %char(UNICODE_DOLLAR) +
                   %char(UNICODE_LEFT_CURLY_BRACE) +
                   %char(UNICODE_RIGHT_CURLY_BRACE);
       tmp = %trim(data : trimChars);

       x = %scan('.' : tmp);
       if (x = 0);
         x = %scan(%char(UNICODE_LEFT_BRACKET) : tmp);
       endif;

       if (x = 0);
         return tmp;
       else;
         return %subst(tmp : 1 : x-1);
       endif;
      /end-free
     P                 E
