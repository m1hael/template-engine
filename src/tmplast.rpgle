     /**
      * \brief Template AST : Main Module
      *
      * \author Mihael Schmidt
      * \date   16.01.2011
      *
      */


     H nomain


      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D next            PR             1A
     D   parser                            likeds(parser_t)
      *
     D back            PR
     D   parser                            likeds(parser_t)
      *
     D more            PR              N
     D   parser                            const likeds(parser_t)
      *
     D nextComment     PR              *
     D   parser                            likeds(parser_t)
      *
     D nextReference...
     D                 PR              *
     D  parser                             likeds(parser_t)
      *
     D nextDirective...
     D                 PR              *
     D   parser                            likeds(parser_t)
      *
     D nextString      PR              *
     D   parser                            likeds(parser_t)
      *
     D getNextDirectiveName...
     D                 PR            10A   varying
     D   parser                            likeds(parser_t)
      *
     D getNextTokenId...
     D                 PR            20I 0
      *
     D resetTokenId    PR
      *
     D skipRestOfLine  PR
     D   parser                            likeds(parser_t)
      *
     D getNextDirectiveExpression...
     D                 PR          1024A   varying
     D   parser                            likeds(parser_t)
      *
      /include 'llist/llist_h.rpgle'
      /include 'lmap/lmap_h.rpgle'
      /include 'unicode_c.rpgle'
      /include 'message/message_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'tmplstor_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'template_h.rpgle'
      /include 'tmplast_h.rpgle'
      /include 'tmpltokn_h.rpgle'
      /include 'tmpltkns_h.rpgle'
      /include 'tmpltknc_h.rpgle'
      /include 'tmpltknr_h.rpgle'


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
     D ast_t           DS                  qualified template
     D   heapId                      10I 0
     D   tokens                        *
      *
     D parser_t...
     D                 DS                  qualified template
     D   data                          *
     D   index                       10I 0
     D   line                        10I 0
     D   state_comment...
     D                                 N


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D CR              C                   x'0D'
     D LF              C                   x'25'
     D NULL            C                   x'00'


      *-------------------------------------------------------------------------
      * Globals
      *-------------------------------------------------------------------------
     D g_tokenId       S             20I 0


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------

     /**
      * \brief Create AST
      *
      * Returns an empty AST.
      *
      * \return Pointer to an AST data structure
      */
     P template_ast_create...
     P                 B                   export
     D                 PI              *
      *
     D ast             S               *
     D header          DS                  likeds(ast_t) based(ast)
      /free
       ast = %alloc(%size(ast_t));

       clear header;
       header.tokens = list_create();

       return ast;
      /end-free
     P                 E


     /**
      * \brief Parse source member
      *
      * \param Template
      * \param Pointer to AST (default: create new AST)
      *
      * \return Pointer to AST
      */
     P template_ast_parser_parse...
     P                 B                   export
     D                 PI              *
     D   template                      *   const
     D   pAst                          *   const options(*nopass)
      *
     D ast             S               *
     D char            S              1A
     D parser          DS                  likeds(parser_t)
     D token           S               *
     D code            S          65535A   varying
      /free
       if (%parms() = 1);
         ast = template_ast_create();
       else;
         ast = pAst;
         template_ast_clear(ast);
       endif;

       resetTokenId();

       parser.data = template_getRawData(template);
       parser.index = -1;

       char = next(parser);
       dow (char <> NULL);

         if (char = TEMPLATE_AST_DIRECTIVE_CHAR);
           back(parser);
           token = nextDirective(parser);

         elseif (char = TEMPLATE_AST_REFERENCE_CHAR);
           back(parser);
           token = nextReference(parser);

         else;
           back(parser);
           token = nextString(parser);

         endif;

         if (token <> *null);
           template_ast_addToken(ast : token);
         endif;

         char = next(parser);

       enddo;

       return ast;
      /end-free
     P                 E


     /**
      * \brief Dispose AST
      *
      * Frees memory used by the AST.
      *
      * \param Pointer to AST
      */
     P template_ast_finalize...
     P                 B                   export
     D                 PI
     D   ast                           *
      *
     D header          DS                  likeds(ast_t) based(ast)
      /free
       if (ast <> *null);
         template_ast_clear(ast);
         list_dispose(header.tokens);
         dealloc(n) ast;
       endif;
      /end-free
     P                 E

     /**
      * \brief Clear AST
      *
      * Clears the AST.
      * \param pointer to the AST
      */
     P template_ast_clear...
     P                 B                   export
     D                 PI
     D   ast                           *   const
     D header          DS                  likeds(ast_t) based(ast)
      *
     D ptr             S               *
     D token           S               *   based(ptr)
      /free
       if (ast <> *null);
         list_abortIteration(header.tokens);
         ptr = list_getNext(header.tokens);
         dow (ptr <> *null);
           template_ast_token_finalize(token);
           ptr = list_getNext(header.tokens);
         enddo;

         list_clear(header.tokens);
       endif;
      /end-free
     P                 E

     /**
      * \brief Adds a token to the AST
      *
      * \param AST
      * \param Token to be added
      */
     P template_ast_addToken...
     P                 B                   export
     D                 PI
     D   ast                           *   const
     D   pToken                        *   const
      *
     D header          DS                  likeds(ast_t) based(ast)
     D token           S               *
      /free
       token = pToken;
       list_add(header.tokens : %addr(token) : %size(token));
      /end-free
     P                 E


     P template_ast_getTokenCount...
     P                 B                   export
     D                 PI            10I 0
     D   ast                           *   const
      *
     D header          DS                  likeds(ast_t) based(ast)
      /free
       return list_size(header.tokens);
      /end-free
     P                 E


     /**
      * \brief Iterate tokens
      *
      * Iterates through the tokens of the AST.
      *
      * \param AST pointer
      *
      * \return Pointer to token data structure or *null no more tokens
      */
     P template_ast_iterateTokens...
     P                 B                   export
     D                 PI              *
     D   ast                           *   const
      *
     D header          DS                  likeds(ast_t) based(ast)
      /free
       return list_getNext(header.tokens);
      /end-free
     P                 E


     /**
      * \brief Remove Token
      *
      * Removes a token from the AST.
      *
      * \param AST pointer
      * \param Token data structure
      */
     P template_ast_removeToken...
     P                 B                   export
     D                 PI
     D  ast                            *   const
     D  pToken                         *   const
      *
     D header          DS                  likeds(ast_t) based(ast)
     D index           S             10I 0
     D token           S               *
      /free
       token = pToken;
       index = list_indexOf(header.tokens : %addr(token) : %size(token));
       list_remove(header.tokens : index);
      /end-free
     P                 E


     /**
      * \brief Next character
      *
      * Returns the next character in the template string and moves the parser
      * forward by one position.
      *
      * \param Pointer to data structure
      *
      * \return next character or null if there are no more characters
      */
     P next            B
     D                 PI             1A
     D  parser                             likeds(parser_t)
      *
     D data            S              1A   based(ptr)
     D retVal          S              1A
      /free
       if (more(parser));
         ptr = parser.data + parser.index + 1;
         retVal = data;
         parser.index += 1;
       else;
         retVal = NULL;
       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Move parser back
      *
      * Moves the position of the parser back by one position.
      *
      * \param Pointer to data structure
      */
     P back            B
     D                 PI
     D  parser                             likeds(parser_t)
      /free
       parser.index -= 1;
      /end-free
     P                 E


     /**
      * \brief Checks for more characters in string
      *
      * Checks if there are still more characters to parse in the given string.
      *
      * \param Pointer to the rest of the template string to be parsed
      */
     P more            B
     D                 PI              N
     D  parser                             const likeds(parser_t)
      /free
       return not (strlen(parser.data) = 0);
      /end-free
     P                 E


     /**
      * \brief Get next comment
      *
      * Return a comment token which contains the text from the current
      * position till the end of the line.
      *
      * \param Parser
      */
     P nextComment     B
     D                 PI              *
     D  parser                             likeds(parser_t)
      *
     D token           S               *
     D char            S              1A
     D indexBackup     S             10I 0
     D data            S          65535A   varying
      /free
       indexBackup = parser.index;

       char = next(parser);
       dow (char <> NULL);

         if (char = CR);

           // check if a LF follows the CR
           char = next(parser);
           if (char <> LF);
             back(parser);
           endif;

           leave;

         elseif (char = LF);
           leave;
         endif;

         data += char;

         char = next(parser);
       enddo;

       data += x'00';

       token = template_ast_token_comment_create(
                   getNextTokenId() :
                   indexBackup :
                   %addr(data : *DATA));

       return token;
      /end-free
     P                 E


     /**
      * \brief Get next reference
      *
      * Returns the next reference. A reference has the syntax of ${name}. If
      * a reference should not be output if it ain't in the context it has the
      * syntax $!{name}.
      *
      * \param Parser
      *
      * \return Token
      *
      * \CPF9898 Following data is not a reference. Template code position is
      *          reset to before this procedure execution.
      */
     P nextReference...
     P                 B
     D                 PI              *
     D  parser                             likeds(parser_t)
      *
     D token           S               *
     D char            S              1A
     D indexBackup     S             10I 0
     D reference       S          65535A   varying
     D quiet           S               N
     D openBraceCount  S              3I 0
      /free
       indexBackup = parser.index;

       char = next(parser);
       reference += char;
       if (char <> UNICODE_DOLLAR);
         message_escape('Invalid character for reference. ' +
             'Syntax: ${name} or $!{name}');
       endif;

       char = next(parser);
       reference += char;
       if (char = UNICODE_EXCLAMATION_MARK);
         quiet = *on;
         char = next(parser);
         reference += char;
       endif;

       if (char <> UNICODE_LEFT_CURLY_BRACE);
         message_escape('Invalid character for reference. ' +
             'Syntax: ${name} or $!{name}');
       endif;

       char = next(parser);
       dow (char <> NULL);

         if (char = UNICODE_LEFT_BRACE);
           openBraceCount += 1;
         elseif (char = UNICODE_RIGHT_BRACE);
           openBraceCount -= 1;
         endif;

         if (char = UNICODE_RIGHT_CURLY_BRACE and openBraceCount = 0);
           reference += char + x'00';

           token = template_ast_token_ref_create(
                       getNextTokenId() :
                       indexBackup :
                       %addr(reference : *DATA));
           if (quiet);
             template_ast_token_ref_setQuiet(token : quiet);
           endif;

           leave;
         endif;

         reference += char;

         char = next(parser);
       enddo;

       return token;
      /end-free
     P                 E


     /**
      * \brief Get next string
      *
      * Returns a token with the string of data from the template for the current
      * position to the next directive, reference or comment.
      *
      * \param Parser
      *
      * \return Token
      *
      */
     P nextString      B
     D                 PI              *
     D   parser                            likeds(parser_t)
      *
     D token           S               *
     D char            S              1A
     D indexBackup     S             10I 0
     D data            S          65535A   varying
      /free
       indexBackup = parser.index;

       char = next(parser);
       dow (char <> NULL);

         if (char = TEMPLATE_AST_DIRECTIVE_CHAR or
             char = TEMPLATE_AST_REFERENCE_CHAR);
           leave;
         endif;

         data += char;

         char = next(parser);
       enddo;

       back(parser);

       data += x'00';

       token = template_ast_token_string_create(
                  getNextTokenId() :
                  indexBackup :
                  %addr(data : *DATA));
       return token;
      /end-free
     P                 E


     P nextDirective...
     P                 B
     D                 PI              *
     D   parser                            likeds(parser_t)
      *
     D token           S               *
     D char            S              1A
     D indexBackup     S             10I 0
     D data            S          65535A   varying
     D directiveName   S             10A   varying
      /free
       char = next(parser);
       if (char <> TEMPLATE_AST_DIRECTIVE_CHAR);
         message_escape('Invalid character for directive. ' +
             'Syntax: #directive-name');
       endif;

       char = next(parser);
       if (char = TEMPLATE_AST_DIRECTIVE_CHAR);
         token = nextComment(parser);
       else;
         back(parser);
         directiveName = getNextDirectiveName(parser);

         // TODO
         //token.id = getNextTokenId();
         //token.type = TEMPLATE_AST_TOKEN_TYPE_DIRECTIVE;
         //token.data = directiveName;
         //token.directiveName = directiveName;
         //token.start = indexBackup;

         if (directiveName = 'end');
           back(parser);
           skipRestOfLine(parser);
         elseif (directiveName = 'else');
           back(parser);
           skipRestOfLine(parser);
         else;
           //token.directiveExpression = getNextDirectiveExpression(parser);
         endif;

       endif;

       return token;
      /end-free
     P                 E


     /**
      * \brief Return directive name
      *
      * \param Parser
      *
      * \return Directive name (in lower case)
      */
     P getNextDirectiveName...
     P                 B
     D                 PI            10A   varying
     D   parser                            likeds(parser_t)
      *
     D name            S             10A   varying
     D char            S              1A
     D indexBackup     S             10I 0
      /free
       indexBackup = parser.index;

       char = next(parser);
       dow (char <> NULL);

         if (char = UNICODE_LEFT_BRACE or char = CR or char = LF);
           leave;
         endif;

         if (char <> *blank);
           name += char;
         endif;

         char = next(parser);
       enddo;

       return name;
      /end-free
     P                 E


     /**
      * \brief Get directive expression
      *
      * Most directives haven an expression like
      * <pre>
      * #set( ${id} = 10 )
      * </pre>
      * In this case "${10} = 10" is the expression
      *
      * \param Parser
      *
      * \return Expression
      */
     P getNextDirectiveExpression...
     P                 B
     D                 PI          1024A   varying
     D   parser                            likeds(parser_t)
      *
     D expression      S           1024A   varying
     D char            S              1A
     D openBraceCount  S              3I 0
      /free
       char = next(parser);
       dow (char <> NULL);

         if (char <> CR and char <> LF);

           if (char = UNICODE_LEFT_BRACE);
             openBraceCount += 1;
           elseif (char = UNICODE_RIGHT_BRACE);
             openBraceCount -= 1;
           endif;

           if (openBraceCount = -1); // reached last closing brace
             leave;
           endif;

           expression += char;

         endif;

         char = next(parser);
       enddo;

       return expression;
      /end-free
     P                 E


     /**
      * \brief Get next token id
      *
      * Return the next token id. This id is unique within the template AST scope.
      *
      * \return Token Id
      */
     P getNextTokenId  B
     D                 PI            20I 0
      /free
       g_tokenId += 1;
       return g_tokenId;
      /end-free
     P                 E


     /**
      * \brief Reset token id counter
      */
     P resetTokenId    B
      /free
       reset g_tokenId;
      /end-free
     P                 E


     P skipRestOfLine  B
     D                 PI
     D   parser                            likeds(parser_t)
      *
     D char            S              1A
      /free
       char = next(parser);

       dow (char <> NULL);

         if (char = CR);

           char = next(parser);
           if (char <> LF);
             back(parser);
           endif;

           leave;

         elseif (char = LF);
           leave;
         endif;

         char = next(parser);
       enddo;
      /end-free
     P                 E


