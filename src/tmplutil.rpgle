
     H nomain


      /include 'tmplutil_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'userspace_h.rpgle'


     /**
      * \brief Create userspace
      *
      * Creates a new userspace, sets the autoextend
      * attribute and returns the pointer to the freshly
      * created userspace.
      *
      * \param Library of the new userspace (optional)
      * \param Filename of the new userspace (optional)
      *
      * \return Pointer to new userspace
      */
     P template_ast_util_getNewUserspace...
     P                 B                   export
     D                 PI              *
     D   pUsLib                      10A   options(*nopass)
     D   pUsName                     10A   options(*nopass)
      *
     D usLib           S             10A
     D usName          S             10A
     D usPtr           S               *
     D usAttr          DS                  likeds(tmpl_userspace_attributes)
      /free
       usLib = 'QTEMP';
       usName = %subst(%str(tmpnam(*omit)) : 7);

       clear QUSEC;
       QUSBPRV = %size(QUSEC);

       userspace_create(
           usName + usLib : *blank : 4096 : x'00' : '*ALL' : 'ILEDocs' :
           '*YES' : QUSEC);
       if (QUSBAVL = 0); // successful

         // set userspace to autoextend
         clear QUSEC;
         QUSBPRV = %size(QUSEC);
         usAttr.size = 1;         // 1 = attribute will be changed
         usAttr.key = 3;          // 3 = extensibility attribute
         usAttr.dataLength = 1;
         usAttr.data = '1';       // 1 = userspace is extensible
         userspace_changeAttributes(usLib : usName + usLib : usAttr : QUSEC);

         // get userspace pointer
         clear QUSEC;
         QUSBPRV = %size(QUSEC);
         userspace_retrievePointer(usName + usLib : usPtr : QUSEC);

         // check if we need to return the userspace lib and name
         if (%parms = 2);
           pUsLib = usLib;
           pUsName = usName;
         endif;

       else;
         // not successful => return null
         usPtr = *null;
       endif;

       return usPtr;
      /end-free
     P                 E


     /**
      * \brief Get record format length
      *
      * Returns the record format length from a source file.
      *
      */
     P template_ast_util_getRecordFormatLength...
     P                 B                   export
     D                 PI            10I 0
     D   library                     10A   const
     D   file                        10A   const
      *
     D usLib           S             10A
     D usName          S             10A
     D usPtr           S               *
     D usData          DS                  likeds(tmpl_userspace_header)
     D                                     based(usPtr)
     D lenOffset       S             10I 0 inz(24)
     D usDataPtr       S               *
     D length          S             10I 0 based(usDataPtr)
      *
     D api_listRecordFormats...
     D                 PR                  extpgm('QUSLRCD')
     D  userspace                    20A   const
     D  format                        8A   const
     D  filename                     20A   const
     D  ovrProcessing                 1A   const
     D  error                              likeds(QUSEC) options(*nopass)
      *
     D retVal          S             10I 0
      /free
       usPtr = template_ast_util_getNewUserspace(usLib : usName);

       if (usPtr <> *null);
         clear QUSEC;
         QUSBPRV = 0;
         api_listRecordFormats(usName + usLib :
                               'RCDL0200' :
                               file + library :
                               '0' :
                               QUSEC);
         usDataPtr = usPtr + usData.offsetList + lenOffset;
         retVal = length - 12; // sequence and date don't count

         clear QUSEC;
         QUSBPRV = %size(QUSEC);
         userspace_delete(usName + usLib : QUSEC);
       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Convert to IFS path
      *
      * Converts the passed library, file and member names to
      * an IFS path.
      *
      * \param Library
      * \param File name
      * \param Member name
      *
      * \return IFS path
      */
     P template_ast_util_getIFSPath...
     P                 B                   export
     D                 PI         65535A   varying
     D  library                      10A   const
     D  file                         10A   const options(*nopass)
     D  member                       10A   const options(*nopass)
      *
     D path            S          65535A   varying
      /free
       path = '/QSYS.LIB/';

       if (library <> 'QSYS');
         path += %trim(library) + '.LIB';
       endif;

       if (%parms() >= 2);
         path += '/' + %trim(file) + '.FILE';
       endif;

       if (%parms() = 3);
         path += '/' + %trim(member) + '.MBR';
       endif;

       return path;
      /end-free
     P                 E



