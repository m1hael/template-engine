
      /if not defined (TEMPLATE_AST_TOKEN_STRING)
      /define TEMPLATE_AST_TOKEN_STRING

     D template_ast_token_string_create...
     D                 PR              *   extproc('template_ast_token_-
     D                                     string_create')
     D   id                          20I 0 const
     D   start                       10I 0 const
     D   data                          *   const
      *
     D template_ast_token_string_finalize...
     D                 PR                  extproc('template_ast_token_-
     D                                     string_finalize')
     D   token                         *
      *
     D template_ast_token_string_getData...
     D                 PR         65535A   varying extproc('template_ast_token_-
     D                                     string_getData')
     D   token                         *   const
      *
     D template_ast_token_string_setData...
     D                 PR                  extproc('template_ast_token_-
     D                                     string_setData')
     D   token                         *   const
     D   type                     65535A   const varying

      /endif

