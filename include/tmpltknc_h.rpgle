
      /if not defined (TEMPLATE_AST_TOKEN_COMMENT)
      /define TEMPLATE_AST_TOKEN_COMMENT

     D template_ast_token_comment_create...
     D                 PR              *   extproc('template_ast_token_-
     D                                     comment_create')
     D   id                          20I 0 const
     D   start                       10I 0 const
     D   data                          *   const
      *
     D template_ast_token_comment_finalize...
     D                 PR                  extproc('template_ast_token_-
     D                                     comment_finalize')
     D   token                         *
      *
     D template_ast_token_comment_getData...
     D                 PR         65535A   varying extproc('template_ast_token_-
     D                                     comment_getData')
     D   token                         *   const
      *
     D template_ast_token_comment_setData...
     D                 PR                  extproc('template_ast_token_-
     D                                     comment_setData')
     D   token                         *   const
     D   type                     65535A   const varying

      /endif

