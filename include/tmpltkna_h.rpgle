      /if not defined (TEMPLATE_AST_TOKEN_ABSTRACT)
      /define TEMPLATE_AST_TOKEN_ABSTRACT

      *-------------------------------------------------------------------------
      * Template Data Structure
      *-------------------------------------------------------------------------
     D template_ast_token_abstract_t...
     D                 DS                  qualified based(nullPointer) align
     D   id                          20I 0
     D   type                         3I 0
     D   start                       10I 0
     D   length                      10I 0


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------
     D template_ast_token_abstract_getType...
     D                 PR             3I 0 extproc('template_ast_token_-
     D                                     abstract_getType')
     D   token                         *   const
      *
     D template_ast_token_abstract_setType...
     D                 PR                  extproc('template_ast_token_-
     D                                     abstract_setType')
     D   token                         *   const
     D   type                         3I 0 const
      *
     D template_ast_token_abstract_getStart...
     D                 PR            10I 0 extproc('template_ast_token_-
     D                                     abstract_getStart')
     D   token                         *   const
      *
     D template_ast_token_abstract_setStart...
     D                 PR                  extproc('template_ast_token_-
     D                                     abstract_setStart')
     D   token                         *   const
     D   start                       10I 0 const
      *
     D template_ast_token_abstract_getLength...
     D                 PR            10I 0 extproc('template_ast_token_-
     D                                     abstract_getLength')
     D   token                         *   const
      *
     D template_ast_token_abstract_setLength...
     D                 PR                  extproc('template_ast_token_-
     D                                     abstract_setLength')
     D   token                         *   const
     D   length                      10I 0 const
      *
     D template_ast_token_abstract_getId...
     D                 PR            20I 0 extproc('template_ast_token_-
     D                                     abstract_getId')
     D   token                         *   const


      /endif

