**FREE

///
// Memory Managment
//
// This module offers memory management functions. All pointers returned from
// the Template Engine procedures should be freed with the proecedures from
// this module as the memory may have been allocated from a user defined heap.
//
// @author Mihael Schmidt
// @project Template Engine
///

/if not defined (TEMPLATE_MEMORY)
/define TEMPLATE_MEMORY

///
// Allocate memory
//
// Allocates a chunk of memory.
//
// @param Size of memory chunk
// @param Pointer to allocated memory
//
// @info The allocated memory should be freed with template_engine_deallocate.
///
dcl-pr template_memory_allocate pointer extproc(*dclcase);
  size int(10) const;
end-pr;

///
// Deallocate memory
//
// Frees the memory pointed at by the passed pointer.
//
// @param Pointer for allocated memory
///
dcl-pr template_memory_deallocate extproc(*dclcase);
  ptr pointer;
end-pr;

///
// Reallocate memory
//
// Resizes the already allocated memory.
//
// @param Pointer for the already allocated memory
// @param New size
// @return Pointer to the resized chunk of memory
//
// @info Only the new pointer should be used because a whole new chunk of
//       memory may have been allocated for the resize operation.
///
dcl-pr template_memory_reallocate pointer extproc(*dclcase);
  ptr pointer;
  size int(10) const;
end-pr;

///
// Discard memory heap
//
// If a user defined heap has been used this heap can be discarded with this
// procedure.
///
dcl-pr template_memory_discard extproc(*dclcase) end-pr;

/endif