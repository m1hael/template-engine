**FREE

///
// Template Context
//
// A template context holds all the data which will be merged with the template.
// <br><br>
// Contexts can be chained (see template_context_setParentContext) so if the
// context does not have a value the request for the value gets passed to the
// parent context.
//
// @author Mihael Schmidt
// @project Template Engine
///

/if not defined (TEMPLATE_CONTEXT)
/define TEMPLATE_CONTEXT

///
// Creates context
//
// Creates an empty template context.
//
// @return Template context
//
// @info The caller must make sure to free the allocated memory of the context
//       by calling template_context_finalize.
///
dcl-pr template_context_create pointer extproc(*dclcase) end-pr;

///
// Dispose template context
//
// Frees all memory allocated for the template context.
//
// @param Template context
///
dcl-pr template_context_finalize extproc(*dclcase);
  templateContext pointer;
end-pr;

///
// Add integer value
//
// Adds an integer value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addIntValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value int(20) const;
end-pr;

///
// Add float value
//
// Adds a float value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addDoubleValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value float(8) const;
end-pr;

///
// Add packed value
//
// Adds a packed value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addDecimalValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value packed(20 : 5) const;
end-pr;

///
// Add character value
//
// Adds a character value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addCharValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value varchar(65535) const;
end-pr;

///
// Add list
//
// Adds a list for the given name to the context.
//
// @param Template context
// @param Key
// @param List
//
// @info Not supported yet!
///
dcl-pr template_context_addListValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value pointer const;
end-pr;

///
// Add map
//
// Adds a map for the given name to the context.
//
// @param Template context
// @param Key
// @param Map
//
// @info Not supported yet!
///
dcl-pr template_context_addMapValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value pointer const;
end-pr;

///
// Add date value
//
// Adds a date value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addDateValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value date const;
end-pr;

///
// Add boolean value
//
// Adds a boolean value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addBooleanValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value ind const;
end-pr;

///
// Add time value
//
// Adds a time value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addTimeValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value time const;
end-pr;

///
// Add timestamp value
//
// Adds a timestamp value for the given name to the context.
//
// @param Template context
// @param Key
// @param Value
///
dcl-pr template_context_addTimestampValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
  value timestamp const;
end-pr;

///
// Remote value
//
// Remove a value with the given name from the context.
//
// @param Template context
// @param Key to be removed
///
dcl-pr template_context_removeValue extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
end-pr;

///
// Get value
//
// Returns a context value either from the passed context or from a chained
// parent context.
//
// @param Template context
// @param Key
//
// @return Value or *blank if no the key does not exist in this context and
//         parent context.
///
dcl-pr template_context_getValue varchar(65535) extproc(*dclcase);
  templateContext pointer const;
  key varchar(100) const;
end-pr;

///
// Set parent context
//
// Creates a chained template context. Any request to the context which cannot
// be answered will get passed to the parent context.
//
// @param Template context
// @param Parent context
///
dcl-pr template_context_setParentContext extproc(*dclcase);
  templateContext pointer const;
  parentContext pointer const;
end-pr;

dcl-pr template_context_pushOnTemplateStack extproc(*dclcase);
  templateContext pointer const;
  template pointer const;
end-pr;

dcl-pr template_context_popTemplateStack pointer extproc(*dclcase);
  templateContext pointer const;
end-pr;

dcl-pr template_context_pushOnDirectiveStack extproc(*dclcase);
  templateContext pointer const;
  directive pointer const;
end-pr;

dcl-pr template_context_popDirectiveStack pointer extproc(*dclcase);
  templateContext pointer const;
end-pr;

/endif