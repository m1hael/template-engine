**FREE

///
// Template Loader
//
// Templates are provided to the template engine via template loaders. A
// template loader provides and manages template "objects".
// <br><br>
// A template loader may decide to reload a template. So before any usage the
// template should be retrieved from the template loader and not cached by the
// application.
// <br><br>
// The default implementation provides a way to load template from stream files
// in the IFS and from a character string on the fly.
//
// @author Mihael Schmidt
// @project Template Engine
///

/if not defined (TEMPLATE_LOADER)
/define TEMPLATE_LOADER

///
// Template for the Template Loader Id
///
dcl-s templateLoaderId_t char(50) template;

///
// Template for the Template Loader "object".
//
// All implementations must always provide all procedure pointers.
///
dcl-ds template_loader_t qualified template align;
  id like(templateLoaderId_t);
  userdata pointer;
  proc_create pointer(*proc);
  proc_finalize pointer(*proc);
  proc_load pointer(*proc);
  proc_needTemplateReload pointer(*proc);
end-ds;

///
// Create template loader
//
// Creates a template loader instance provided by the passed service program.
//
// @param Service program name
// @param User data (optional)
// @param Procedure name (optional)
//
// @return Template loader "object"
//
// @info The memory of the template loader instance must be freed after usage
//       with template_loader_finalize.
///
dcl-pr template_loader_create pointer extproc(*dclcase);
   serviceProgram char(10) const;
   userData pointer const options(*nopass);
   procedureName char(256) const options(*nopass);
end-pr;

///
// Load template
//
// Loads a template identified by the passed name.
//
// @param Template loader
// @param Template Id
//
// @return Template
//
// @info The memory for the template "object" must to be explictly
//       freed after usage.
///
dcl-pr template_loader_load pointer extproc(*dclcase);
   templateLoader pointer const;
   templateName char(100) const;
end-pr;

///
// Free template loader memory
//
// Frees all memory allocated by and for the template loader.
//
// @param Template loader
///
dcl-pr template_loader_finalize extproc(*dclcase);
   templateLoader pointer;
end-pr;

///
// Get template loader id
//
// Returns the Id of the template loader.
//
// @param Template loader
//
// @return Template loader id
///
dcl-pr template_loader_getId char(50) extproc(*dclcase);
   templateLoader pointer const;
end-pr;

///
// Check for template reload
//
// Determines if the template needs to be reloaded.
//
// @param Template loader
// @param Template
//
// @return *on if the template changed and needs reloading
///
dcl-pr template_loader_needTemplateReload ind extproc(*dclcase);
  templateLoader pointer const;
  template pointer const;
end-pr;

///
// Create template loader for stream files
//
// Creates a template loader instance for stream files. The passed file path
// points to a directory containing the templates.
//
// @param Absolute file path to the template directory (null-terminated)
//
// @return Template loader "object"
//
// @info The memory of the template loader instance must be freed after usage
//       with template_loader_finalize.
///
dcl-pr template_loader_stmf_create pointer extproc(*dclcase);
  path pointer const options(*nopass : *string);
end-pr;

///
// Load stream file template
//
// Loads a template identified by the passed name.
//
// @param Template loader
// @param Template file name
//
// @return Template
//
// @info The memory for the template "object" must to be explictly
//       freed after usage.
///
dcl-pr template_loader_stmf_load pointer extproc(*dclcase);
  templateLoader pointer const;
  templateName char(100) const;
end-pr;

///
// Free template loader memory
//
// Frees all memory allocated by and for the template loader.
//
// @param Template loader
///
dcl-pr template_loader_stmf_finalize extproc(*dclcase);
  templateloader pointer;
end-pr;

///
// Check for template reload
//
// Determines if the template needs to be reloaded.
//
// @param Template loader
// @param Template
//
// @return *on if template needs to be reloaded else *off
///
dcl-pr template_loader_stmf_needTemplateReload ind extproc(*dclcase);
   templateLoader pointer const;
   template pointer const;
end-pr;

///
// Get template loader id
//
// Returns the Id of the template loader.
//
// @param Template loader
//
// @return Template loader id
///
dcl-pr template_loader_stmf_getId char(50) extproc(*dclcase);
  templateLoader pointer const;
end-pr;

///
// Create memory template loader
//
// Creates a template loader instance with the passed template string.
//
// @param Template string (null-terminated)
//
// @return Template loader "object"
//
// @info The memory of the template loader instance must be freed after usage
//       with template_loader_finalize.
///
dcl-pr template_loader_memory_create pointer extproc(*dclcase);
  templateString pointer const options(*nopass : *string);
end-pr;

///
// Load template
//
// Returns a template "object" with the template string passed on creation of
// the template loader.
//
// @param Template loader
// @param Template name (which is not important in this implemenation)
//
// @return Template "object"
//
// @info The memory for the template "object" must to be explictly
//       freed after usage.
///
dcl-pr template_loader_memory_load pointer extproc(*dclcase);
  templateLoader pointer const;
  templateName char(100) const;
end-pr;

///
// Free template loader memory
//
// Frees all memory allocated by and for the template loader.
//
// @param Template loader
///
dcl-pr template_loader_memory_finalize extproc(*dclcase);
  templateloader pointer;
end-pr;

///
// Check for template reload
//
// Determines if the template needs to be reloaded. This implemenation never
// reloads the template.
//
// @param Template loader
// @param Template
//
// @return always *off as the used template string never changes
///
dcl-pr template_loader_memory_needTemplateReload ind extproc(*dclcase);
  templateLoader pointer const;
  template pointer const;
end-pr;

///
// Get template loader id
//
// Returns the Id of the template loader.
//
// @param Template loader
//
// @return Template loader id
///
dcl-pr template_loader_memory_getId char(50) extproc(*dclcase);
  templateLoader pointer const;
end-pr;

/endif