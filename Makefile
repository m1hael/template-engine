#
# Build script for Template Engine
#


#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# The rpg modules and the binder source file are also created in BIN_LIB.
# Binder source file and rpg module can be remove with the clean step 
# (make clean).
BIN_LIB=TMPLENGINE

# The directory where all dependencies have their copy books.
INCDIR=/usr/local/include

#
# The folder where the copy books for the template engine will be copied to with
# the install step (make install).
#
USRINCDIR=/usr/local/include

#
# User-defined part end
#-------------------------------------------------------------------------------


all: clean compile bind

compile: $(OBJECTS)
	cd src && make compile

clean: .PHONY
	cd src && make clean

bind: .PHONY
	cd src && make bind

install: .PHONY
	-mkdir $(USRINCDIR)/template
	cp include/tmplengi_h.rpgle $(USRINCDIR)/template/
	cp include/tmplload_h.rpgle $(USRINCDIR)/template/
	cp include/tmplmem_h.rpgle $(USRINCDIR)/template/
	cp include/tmplutil_h.rpgle $(USRINCDIR)/template/
	cp include/tmplcont_h.rpgle $(USRINCDIR)/template/
	cp include/template_engine_h.rpgle $(USRINCDIR)/template/

.PHONY:
